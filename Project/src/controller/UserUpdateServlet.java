package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.findById(id);



		request.setAttribute("user", user);
		request.setAttribute("id", id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kosin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("pass");
		String password2 = request.getParameter("pass2");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");
		String id = request.getParameter("id");

        java.sql.Date  birth_date = java.sql.Date.valueOf(birth);
        int i = Integer.parseInt(id);

      //登録失敗
        boolean error = false;

        if(name.equals("") || birth.equals("")) {
			error = true;
		}

		if(!password.equals(password2)) {
			error = true;
		}
		if(error) {
			response.sendRedirect("UserUpdateServlet");
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			return;
		}

		//登録成功
		String password3 = request.getParameter("pass3");

		if(password.equals("") && password2.equals("")) {

			UserDao userDao = new UserDao();
			User c = new User(password3, name, birth_date, i);

			userDao.update(c);
			response.sendRedirect("UserListServlet");
			return;
		}
		try {
	        //ハッシュを生成したい元の文字列
	        String source = password;
	        //ハッシュ生成前にバイト配列に置き換える際のCharset
	        Charset charset = StandardCharsets.UTF_8;
	        //ハッシュアルゴリズム
	        String algorithm = "MD5";
	        //ハッシュ生成処理
	        byte[] bytes;

			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

	        String result = DatatypeConverter.printHexBinary(bytes);

	        UserDao userDao = new UserDao();
			User c = new User(result, name, birth_date, i);

			userDao.update(c);
			response.sendRedirect("UserListServlet");


		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
