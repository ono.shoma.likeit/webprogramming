<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
    <header>
     <form action="UserInsertServlet" method="post">
        <div align="right">
            <h6>${userInfo.name} さん</h6>
            <a href="LogoutServlet"class="red">ログアウト</a>
        </div>
        </header>
        <h3 align="center">ユーザー新規登録</h3>
        <div align="center"class="red">${errMsg}</div>
        <table align="center" border="0" >
            <tr align="center"class=height>
                <td width="400">ログインID</td>
                <td width="400"><input type="text" name="id"></td>
            </tr>
            <tr align="center"class=height>
                <td width="400">パスワード</td>
                <td width="400"><input type="text" name="pass"></td>
            </tr>
            <tr align="center"class=height>
                <td width="400">パスワード（確認）</td>
                <td width="400"><input type="text" name="pass2"></td>
            </tr>
            <tr align="center"class=height>
                <td width="400">ユーザー名</td>
                <td width="400"><input type="text" name="name"></td>
            </tr>
            <tr align="center"class=height>
                <td width="400">生年月日</td>
                <td width="400"><input type="date" name="birth_date"></td>
            </tr>
        </table>
        <div align="center"><input type="submit" class=padding50 value="登録"></div>
        </form>
        <div class=user><a href="UserListServlet">戻る</a></div>
    </body>
</html>