<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
        <header>
          <div align="right">
            <h6>${userInfo.name} さん</h6>
            <a href="LogoutServlet"class="red">ログアウト</a>
        </div>
        </header>
        <h3 align="center">ユーザー削除確認</h3>
        <h5 class="margin600 marginTop20">ログインID:${user.loginId}</h5>
        <h5 class="margin600">を本当に削除してもよろしいでしょうか。</h5>
        <table align="center" border="0" class="marginTop100">
            <tr align="center"class=height>
                <td width="400"><form action=UserListServlet method=get>
                    <div align="center"><input type="submit" class=padding50 value="キャンセル">
                    </div>
                    </form>
                </td>
                <td width="400"><form action=UserDeleteServlet method=post>
               	 <input type="hidden" name="id" value="${id}">
                    <div align="center"><input type="submit" class=padding50 value="OK">
                    </div>
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>