<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="css/style.css">
</head>
	<body>
        <header>
        <div align="right">
            <h6>${userInfo.name} さん</h6>
            <a href="LogoutServlet" class="red">ログアウト</a>
        </div>
        </header>
    <h3 align="center">ユーザー覧</h3>
    <div class=logout><a href="UserInsertServlet?id=${user.id}">新規登録</a></div>
    <form action="UserListServlet"method="post">
        <p align="center">ログインID<input type="text" name="id"></p>
        <p align="center">ユーザー名<input type="text" name="name"></p>
        <table align="center" border="0">
        <tr>
            <td><label for="start">生年月日</label>
                  <input type="date" id="start" name="trip-start"
       　
                         min="1950-01-01" max="2030-12-31"></td>
            <td><label for="start">〜</label>
                  <input type="date" id="start" name="trip-start2"
       　
                         min="1950-01-01" max="2030-12-31"></td>
        </tr>
        </table>
        <table align="center" border="1">
            <p align="right"class=width1100><input type="submit"class=padding50 value="検索"></p>
        </form>
    <tr>
        <th width="100">ログインID</th>
        <th width="100">ユーザー名</th>
        <th width="100">生年月日</th>
        <th width="300"></th>
    </tr>
    <c:forEach var="user" items="${userList}">
    <tr>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
        <td>
         <a href="UserDetailServlet?id=${user.id}" class="btn btn-primary margin36_10">詳細</a>
          <c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
            <a href="UserUpdateServlet?id=${user.id}" class="btn btn-success margin10_10">更新</a>
          </c:if>
          <c:if test="${userInfo.loginId == 'admin'}">
             <a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger margin10_36">削除</a>
          </c:if>
        </td>
       </tr>
      </c:forEach>
     </table>
	</body>
</html>
