<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>更新</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="css/style.css">
</head>
	<body>
        <header>
       <div align="right">
            <h6>${userInfo.name} さん</h6>
            <a href="LogoutServlet"class="red">ログアウト</a>
        </div>
        </header>
		<h3 align="center">ユーザー情報更新</h3>
		<div align="center" class=red>${errMsg }</div>
       <form action=UserUpdateServlet method="post">
       <input type="hidden" name="id" value="${id}">
       <input type="hidden" name="pass3" value="${user.password}">
        <table align="center" border="0">
            <tr align="left" class=height>
                <td class=padding>ログインID</td>
                <td>${user.loginId}</td>
            </tr>
            <tr align="left"class=height>
                <td>パスワード</td>
                <td><input type="password" name="pass"></td>
            </tr>
            <tr align="left"class=height>
                <td>パスワード（確認）</td>
                <td><input type="password" name="pass2"></td>
            </tr>
            <tr align="left"class=height>
                <td>ユーザー名</td>
                <td><input type="text" name="name" value=${user.name }></td>
            </tr>
            <tr align="left"class=height>
                <td>生年月日</td>
                <td><input type="date" name="birth" value=${user.birthDate }></td>
            </tr>
        </table>
        <div align="center"><input type="submit" class=padding50 value="更新"></div>
        </form>
        <div class=user><a href="UserListServlet">戻る</a></div>
	</body>
</html>
