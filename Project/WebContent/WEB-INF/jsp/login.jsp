<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
	 <link rel="stylesheet" href="css/style.css">
    </head>
	<body>
        <form action="LoginServlet" method="post">
        <h3 align="center">ログイン画面</h3>
        <p align="center">ログインID<input type="text" name="id"></p>
        <p align="center">パスワード<input type="password" name="pass"></p>
        <p align="center"><input type="submit" value="ログイン"></p>
        <p align="center" class="red">${errMsg}</p>
        </form>
    </body>
</html>

