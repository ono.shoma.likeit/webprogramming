<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報詳細</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="css/style.css">
</head>
	<body>
        <header>
        <div align="right">
            <h6>${userInfo.name} さん</h6>
            <a href="LogoutServlet"class="red">ログアウト</a>
        </div>
        </header>
		<h3 align="center">ユーザー情報詳細参照</h3>
        <table align="center" border="0">
            <tr align="left" class=height>
                <td class=padding>ログインID</td>
                <td>${user.loginId}</td>
            </tr>
            <tr align="left"class=height>
                <td>ユーザー名</td>
                <td>${user.name}</td>
            </tr>
            <tr align="left"class=height>
                <td>生年月日</td>
                <td>${user.birthDate}</td>
            </tr>
            <tr align="left"class=height>
                <td>登録日時</td>
                <td>${user.createDate}</td>
            </tr>
            <tr align="left"class=height>
                <td>更新日時</td>
                <td>${user.updateDate}</td>
            </tr>
        </table>
        <div class=user><a href="UserListServlet">戻る</a></div>
	</body>
</html>
